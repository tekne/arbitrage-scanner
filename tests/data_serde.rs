use arbitrage_scanner::deribit::BookSummaryByCurrency;
use pretty_assertions::assert_eq;
use serde_json::Value;

#[test]
fn get_book_summary_by_currency() {
    let s = include_str!("get_book_summary_by_currency.json");
    let v: Value =
        serde_json::from_str(s).expect("get_book_summary_by_currency.json is valid JSON");
    let b: BookSummaryByCurrency = serde_json::from_value(v)
        .expect("get_book_summary_by_currency.json is an order book summary");
    let v = serde_json::to_string(&b).expect("serializing an order book summary works");
    let b2: BookSummaryByCurrency =
        serde_json::from_str(&v).expect("serialized order book summaries yields valid JSON");
    assert_eq!(
        serde_json::to_string(&b2).expect("serializing a reconstructed order book works"),
        v
    );
    // We don't compare b and b2 directly because while they return the same `Debug` representation they don't actually compare equal
    // This is probably just floating point stuff, but investigate...
    // assert_eq!(b, b2);
    assert_eq!(
        format!("{:?}", b),
        format!("{:?}", b2),
    );

    let mut fake_file: Vec<u8> = Vec::new();
    rmp_serde::encode::write(&mut fake_file, &b2).expect("MessagePack serde to a fake file should work");
    let b3: BookSummaryByCurrency = rmp_serde::decode::from_read(&fake_file[..]).expect("MessagePack deserialization should work");
    assert_eq!(b2, b3);
}
