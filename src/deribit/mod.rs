use crate::Error::*;
use chrono::Month;
use ordered_float::NotNan;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::fmt::{self, Debug, Display, Formatter};

pub mod data;
use data::*;

/// Configuration for accessing Deribit
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Deribit {
    /// Whether this connection should use the testnet
    test: bool,
}

impl Deribit {
    /// Create a new production configuration
    pub const fn production() -> Deribit {
        Deribit { test: false }
    }
    /// Create a new test configuration
    pub const fn test() -> Deribit {
        Deribit { test: true }
    }
}

impl Deribit {
    /// Get the Deribit URL associated with this configuration
    pub const fn get_deribit_url(&self) -> &str {
        if self.test {
            "https://test.deribit.com/api/v2"
        } else {
            "https://www.deribit.com/api/v2"
        }
    }
    /// Retrieve order book summary information for all instruments for a currency, optionally filtered by kind
    pub async fn get_book_summary_by_currency(
        &self,
        currency: Currency,
        kind: Option<InstrumentKind>,
    ) -> Result<BookSummaryByCurrency, crate::error::Error> {
        let base_url = self.get_deribit_url();
        let request_url = if let Some(kind) = kind {
            format!(
                "{base_url}/public/get_book_summary_by_currency?currency={currency}&kind={kind}",
                base_url = base_url,
                currency = currency,
                kind = kind
            )
        } else {
            format!(
                "{base_url}/public/get_book_summary_by_currency?currency={currency}",
                base_url = base_url,
                currency = currency
            )
        };
        let response = reqwest::get(&request_url)
            .await
            .map_err(DeribitRequestError)?;
        Ok(response.json().await.map_err(JSONDeserializationError)?)
    }
    /// Retrieve order book summary information for a specific instrument
    pub async fn get_book_summary_by_instrument(
        &self,
        instrument_name: Instrument,
    ) -> Result<Value, anyhow::Error> {
        let request_url = format!(
            "{url}/v2/public/get_book_summary_by_instrument?instrument_name={instrument_name}",
            url = self.get_deribit_url(),
            instrument_name = instrument_name
        );
        let response = reqwest::get(&request_url).await?;
        Ok(response.json().await?)
    }
    /// Returns available financial instruments
    pub async fn get_instruments(
        &self,
        currency: Currency,
        kind: Option<InstrumentKind>,
        expired: Option<bool>,
    ) -> Result<Value, crate::error::Error> {
        let base_url = self.get_deribit_url();
        let request_url = match (kind, expired) {
            (None, None) => format!(
                "{base_url}/public/get_instruments?currency={currency}",
                base_url = base_url,
                currency = currency,
            ),
            (Some(kind), None) => format!(
                "{base_url}/public/get_instruments?currency={currency}&kind={kind}",
                base_url = base_url,
                currency = currency,
                kind = kind,
            ),
            (None, Some(expired)) => format!(
                "{base_url}/public/get_instruments?currency={currency}&expired={expired}",
                base_url = base_url,
                currency = currency,
                expired = expired,
            ),
            (Some(kind), Some(expired)) => format!(
                "{base_url}/public/get_instruments?currency={currency}&kind={kind}&expired={expired}",
                base_url = base_url,
                currency = currency,
                kind = kind,
                expired = expired,
            ),
        };
        let response = reqwest::get(&request_url)
            .await
            .map_err(DeribitRequestError)?;
        Ok(response.json().await.map_err(JSONDeserializationError)?)
    }
    /// Returns available options
    pub async fn get_options(
        &self,
        currency: Currency,
        expired: Option<bool>,
    ) -> Result<Value, crate::Error> {
        self.get_instruments(currency, Some(InstrumentKind::Option), expired)
            .await
    }
    /// Get the order book, along with other market values, for a given instrument
    pub async fn get_order_book(
        &self,
        instrument_name: Instrument,
        depth: Option<u64>,
    ) -> Result<Value, crate::Error> {
        let base_url = self.get_deribit_url();
        let request_url = if let Some(depth) = depth {
            format!(
                "{base_url}/public/get_order_book?depth={depth}&instrument_name={instrument_name}",
                base_url = base_url,
                instrument_name = instrument_name,
                depth = depth
            )
        } else {
            format!(
                "{base_url}/public/get_order_book?instrument_name={instrument_name}",
                base_url = base_url,
                instrument_name = instrument_name,
            )
        };
        let response = reqwest::get(&request_url)
            .await
            .map_err(DeribitRequestError)?;
        Ok(response.json().await.map_err(JSONDeserializationError)?)
    }
    /// Get the index price for a given currency
    pub async fn get_index_price(&self, currency: Currency) -> Result<GetIndexPrice, crate::Error> {
        let one = NotNan::new(1.0).expect("1 is not NaN");
        let index_name = match currency {
            Currency::BTC => "btc_usd",
            Currency::ETH => "eth_usd",
            //TODO: think about this...
            Currency::USDT => {
                return Ok(GetIndexPrice {
                    id: None,
                    result: IndexPrice {
                        estimated_delivery_price: one,
                        index_price: one,
                    },
                })
            }
        };
        let base_url = self.get_deribit_url();
        let request_url = format!(
            "{base_url}/public/get_index_price?index_name={index_name}",
            base_url = base_url,
            index_name = index_name
        );
        let response = reqwest::get(&request_url)
            .await
            .map_err(DeribitRequestError)?;
        Ok(response.json().await.map_err(JSONDeserializationError)?)
    }
}

/// A response to a call to `get_book_summary_by_currency`
#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct BookSummaryByCurrency {
    /// The ID of this response, if any
    #[serde(default)]
    pub id: Option<u64>,
    /// The result of this response
    pub result: Vec<BookSummary>,
}

/// A response to a call to `get_index_price`
#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct GetIndexPrice {
    /// The ID of this response, if any
    #[serde(default)]
    pub id: Option<u64>,
    /// The result of this response
    pub result: IndexPrice,
}

/// An index price
#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct IndexPrice {
    /// The estimated delivery price for the market
    estimated_delivery_price: NotNan<f64>,
    /// The value of the requested index
    index_price: NotNan<f64>,
}

#[cfg(test)]
mod test {
    use super::*;
    #[tokio::test]
    async fn get_book_summary_by_currency() {
        let deribit = Deribit::production();
        let _summaries = deribit
            .get_book_summary_by_currency(Currency::BTC, Some(InstrumentKind::Option))
            .await
            .unwrap();
    }
}
