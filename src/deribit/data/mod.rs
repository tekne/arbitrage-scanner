use super::*;
use fxhash::FxHashMap;
use std::borrow::Borrow;
use std::collections::BTreeMap;

pub mod parse;

/// A currency supported by Deribit
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum Currency {
    /// Tether US Dollar tokens
    #[serde(rename = "USD")]
    USDT,
    /// Bitcoin
    BTC,
    /// Ethereum
    ETH,
}

pub use Currency::*;

/// A financial instrument supported by Deribit
#[derive(Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum InstrumentKind {
    /// A future
    #[serde(rename = "future")]
    Future,
    /// An option
    #[serde(rename = "option")]
    Option,
}

/// A financial instrument supported by Deribit
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub enum Instrument {
    /// A future
    Future(FutureContract),
    /// A perpetual
    Perpetual(PerpetualContract),
    /// An option
    Option(OptionContract),
}

impl Instrument {
    /// The currency of this instrument
    pub const fn currency(self) -> Currency {
        match self {
            Instrument::Future(this) => this.currency,
            Instrument::Perpetual(this) => this.currency,
            Instrument::Option(this) => this.currency,
        }
    }
    /// The date of this instrument, options and futures only
    pub const fn date(self) -> Option<ContractDate> {
        match self {
            Instrument::Future(this) => Some(this.date),
            Instrument::Perpetual(_) => None,
            Instrument::Option(this) => Some(this.date),
        }
    }
    /// The kind of this instrument
    pub const fn kind(self) -> InstrumentKind {
        match self {
            Instrument::Future(_) | Instrument::Perpetual(_) => InstrumentKind::Future,
            Instrument::Option(_) => InstrumentKind::Option,
        }
    }
}

/// An underlying index
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub enum UnderlyingIndex {
    /// An instrument
    Instrument(Instrument),
    /// An index price
    IndexPrice,
}

/// A futures contract
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct FutureContract {
    /// Whether this future is synthetic
    pub syn: bool,
    /// The currency of this future
    pub currency: Currency,
    /// The expiration of this future
    pub date: ContractDate,
}

/// A perpetual contract
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct PerpetualContract {
    /// The currency of this perpetual
    pub currency: Currency,
}

/// A kind of option
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub enum OptionKind {
    /// A put option
    Put,
    /// A call option
    Call,
}

/// A contract date
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct ContractDate {
    /// The day of the month
    pub day: u8,
    /// The month
    pub month: Month,
    /// The year, counting from 0
    pub year: u16,
}

/// An options contract
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct OptionContract {
    /// The currency of this option
    pub currency: Currency,
    /// The expiration of this option
    pub date: ContractDate,
    /// The strike price of this option
    pub strike: NotNan<f64>,
    /// The kind of this option
    pub kind: OptionKind,
}

/// A summary of the order book for a given instrument
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct BookSummary {
    pub ask_price: Option<NotNan<f64>>,
    pub base_currency: Currency,
    pub bid_price: Option<NotNan<f64>>,
    pub creation_timestamp: i64,
    pub current_funding: Option<NotNan<f64>>,
    pub estimated_delivery_price: Option<NotNan<f64>>,
    pub funding_8h: Option<NotNan<f64>>,
    pub high: Option<NotNan<f64>>,
    pub instrument_name: Instrument,
    pub interest_rate: Option<NotNan<f64>>,
    pub last: Option<NotNan<f64>>,
    pub low: Option<NotNan<f64>>,
    pub mark_price: Option<NotNan<f64>>,
    pub mid_price: Option<NotNan<f64>>,
    pub open_interest: NotNan<f64>,
    pub price_change: Option<NotNan<f64>>,
    pub quote_currency: Currency,
    pub underlying_index: Option<UnderlyingIndex>,
    pub volume: NotNan<f64>,
    pub volume_usd: Option<NotNan<f64>>,
}

impl BookSummary {
    pub fn equivalent(&self, other: &BookSummary) -> bool {
        let mut this = *self;
        let mut other = *other;
        this.creation_timestamp = 0;
        other.creation_timestamp = 0;
        this == other
    }
    pub fn floats(&self) -> impl Iterator<Item = NotNan<f64>> {
        std::array::IntoIter::new([
            self.ask_price.unwrap_or_default(),
            self.bid_price.unwrap_or_default(),
            self.current_funding.unwrap_or_default(),
            self.estimated_delivery_price.unwrap_or_default(),
            self.high.unwrap_or_default(),
            self.interest_rate.unwrap_or_default(),
            self.last.unwrap_or_default(),
            self.low.unwrap_or_default(),
            self.mark_price.unwrap_or_default(),
            self.mid_price.unwrap_or_default(),
            self.open_interest,
            self.price_change.unwrap_or_default(),
            self.volume,
            self.volume_usd.unwrap_or_default(),
        ])
    }
}

/// Get all box spreads for puts and calls at a given strike price
pub fn box_spreads<I>(currency: Currency, summaries: I) -> impl Iterator<Item = BoxSpread>
where
    I: Iterator,
    I::Item: std::borrow::Borrow<BookSummary>,
{
    let mut prices: FxHashMap<ContractDate, BTreeMap<NotNan<f64>, (BidAsk, BidAsk)>> =
        FxHashMap::default();

    let mut high_estimated_delivery: Option<NotNan<f64>> = None;
    let mut low_estimated_delivery: Option<NotNan<f64>> = None;

    for summary in summaries {
        let summary = summary.borrow();
        if summary.base_currency != currency {
            continue;
        }
        match summary.instrument_name {
            Instrument::Option(option) => {
                let strike = option.strike;
                let prices = prices.entry(option.date).or_default();
                let entry = prices.entry(strike).or_default();
                let bid_ask = BidAsk {
                    bid: summary.bid_price,
                    ask: summary.ask_price,
                };
                match option.kind {
                    OptionKind::Call => {
                        entry.0 = bid_ask;
                    }
                    OptionKind::Put => {
                        entry.1 = bid_ask;
                    }
                }
            }
            _ => {}
        }
        if let Some(estimated_delivery) = summary.estimated_delivery_price {
            let hi = high_estimated_delivery.get_or_insert(estimated_delivery);
            let lo = low_estimated_delivery.get_or_insert(estimated_delivery);
            *hi = (*hi).max(estimated_delivery);
            *lo = (*lo).min(estimated_delivery);
        }
    }
    prices
        .into_iter()
        .map(move |(date, prices)| {
            prices
                .iter()
                .map(|(&high_strike, &(high_call, high_put))| {
                    prices
                        .iter()
                        .take_while(move |(&low_strike, _)| low_strike < high_strike)
                        .map(move |(&low_strike, &(low_call, low_put))| BoxSpread {
                            currency,
                            date,
                            high_strike,
                            low_strike,
                            high_call,
                            high_put,
                            low_call,
                            low_put,
                            high_estimated_delivery,
                            low_estimated_delivery,
                        })
                })
                .flatten()
                .collect::<Vec<_>>() //TODO: remove allocation
        })
        .flatten()
}

/// A bid-ask spread
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Default, Serialize, Deserialize)]
pub struct BidAsk {
    /// The bid of this spread
    pub bid: Option<NotNan<f64>>,
    /// The ask of this spread
    pub ask: Option<NotNan<f64>>,
}

/// A side in a trade
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum Side {
    /// Long-side, i.e. buy
    Long,
    /// Short-side, i.e. sell
    Short,
}

/// A box spread
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct BoxSpread {
    /// The currency of this box spread
    pub currency: Currency,
    /// The expiration of this box spread
    pub date: ContractDate,
    /// The higher strike price of this box spread; with prices in USD
    pub high_strike: NotNan<f64>,
    /// The lower strike price of this box spread; with prices in USD
    pub low_strike: NotNan<f64>,
    /// The high call bid/ask spread; with prices in the currency
    pub high_call: BidAsk,
    /// The high put bid/ask spread; with prices in the currency
    pub high_put: BidAsk,
    /// The low call bid/ask spread; with prices in the currency
    pub low_call: BidAsk,
    /// The low put bid/ask spread; with prices in the currency
    pub low_put: BidAsk,
    /// The highest recorded estimated delivery price
    pub high_estimated_delivery: Option<NotNan<f64>>,
    /// The lowest recorded estimated delivery price
    pub low_estimated_delivery: Option<NotNan<f64>>,
}

impl BoxSpread {
    /// Get the premium for going long one box spread, in the box spread's currency
    pub fn long_premium(&self) -> Option<NotNan<f64>> {
        Some(self.high_call.bid? + self.low_put.bid? - self.low_call.ask? - self.high_put.ask?)
    }
    /// Get the premium of going short one box spread, in the box spread's currency
    pub fn short_premium(&self) -> Option<NotNan<f64>> {
        Some(self.low_call.bid? + self.high_put.bid? - self.high_call.ask? - self.low_put.ask?)
    }
    /// Get the value at expiration of this box spread
    pub fn expiration_value(&self) -> NotNan<f64> {
        self.high_strike - self.low_strike
    }
    /// Get the profit of going long this box spread in USD
    pub fn long_profit(&self) -> Option<NotNan<f64>> {
        Some(self.expiration_value() - self.long_premium()? * self.low_estimated_delivery?)
    }
    /// Get the profit of going short this box spread in USD
    pub fn short_profit(&self) -> Option<NotNan<f64>> {
        Some(self.short_premium()? * self.high_estimated_delivery? - self.expiration_value())
    }
    /// Check whether this box spread is profitable to long or short with more than epsilon of profit
    pub fn profitable(&self, epsilon: NotNan<f64>) -> Option<(Side, NotNan<f64>)> {
        if let Some(profit) = self.long_profit() {
            if profit > epsilon {
                return Some((Side::Long, profit));
            }
        }
        if let Some(profit) = self.short_profit() {
            if profit > epsilon {
                return Some((Side::Short, profit));
            }
        }
        None
    }
}
