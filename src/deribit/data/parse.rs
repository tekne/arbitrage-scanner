use std::str::FromStr;

use super::*;
use nom::{
    branch::*, bytes::complete::tag, character::complete::digit1, combinator::*, sequence::*,
    IResult,
};
use serde::de::Error;

pub fn currency(input: &str) -> IResult<&str, Currency> {
    alt((
        map(tag("USDT"), |_| Currency::USDT),
        map(tag("USD"), |_| Currency::USDT),
        map(tag("BTC"), |_| Currency::BTC),
        map(tag("ETH"), |_| Currency::ETH),
    ))(input)
}

impl FromStr for Currency {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (rest, value) = currency(s).map_err(|_| ())?;
        if rest == "" {
            Ok(value)
        } else {
            Err(())
        }
    }
}

pub fn instrument(input: &str) -> IResult<&str, Instrument> {
    alt((
        map(option_contract, Instrument::Option),
        map(future_contract, Instrument::Future),
        map(perpetual_contract, Instrument::Perpetual),
    ))(input)
}

impl FromStr for Instrument {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (rest, value) = instrument(s).map_err(|_| ())?;
        if rest == "" {
            Ok(value)
        } else {
            Err(())
        }
    }
}

impl<'de> Deserialize<'de> for Instrument {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s: String = Deserialize::deserialize(deserializer)?;
        Instrument::from_str(&*s)
            .map_err(|_| D::Error::custom(format!("Invalid instrument {:?}", s)))
    }
}

pub fn underlying_index(input: &str) -> IResult<&str, UnderlyingIndex> {
    alt((
        map(instrument, UnderlyingIndex::Instrument),
        map(tag("index_price"), |_| UnderlyingIndex::IndexPrice),
    ))(input)
}

impl FromStr for UnderlyingIndex {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (rest, value) = underlying_index(s).map_err(|_| ())?;
        if rest == "" {
            Ok(value)
        } else {
            Err(())
        }
    }
}

impl<'de> Deserialize<'de> for UnderlyingIndex {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s: String = Deserialize::deserialize(deserializer)?;
        UnderlyingIndex::from_str(&*s)
            .map_err(|_| D::Error::custom(format!("Invalid instrument {:?}", s)))
    }
}

pub fn perpetual_contract(input: &str) -> IResult<&str, PerpetualContract> {
    map(terminated(currency, tag("-PERPETUAL")), |currency| {
        PerpetualContract { currency }
    })(input)
}

impl FromStr for PerpetualContract {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (rest, value) = perpetual_contract(s).map_err(|_| ())?;
        if rest == "" {
            Ok(value)
        } else {
            Err(())
        }
    }
}

pub fn future_contract(input: &str) -> IResult<&str, FutureContract> {
    map(
        tuple((opt(tag("SYN.")), currency, tag("-"), contract_date)),
        |(syn, currency, _, date)| FutureContract {
            syn: syn.is_some(),
            currency,
            date,
        },
    )(input)
}

impl FromStr for FutureContract {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (rest, value) = future_contract(s).map_err(|_| ())?;
        if rest == "" {
            Ok(value)
        } else {
            Err(())
        }
    }
}

pub fn option_contract(input: &str) -> IResult<&str, OptionContract> {
    map(
        tuple((
            currency,
            tag("-"),
            contract_date,
            tag("-"),
            map_res(digit1, FromStr::from_str),
            tag("-"),
            option_kind,
        )),
        |(currency, _, date, _, strike, _, kind)| OptionContract {
            currency,
            date,
            strike,
            kind,
        },
    )(input)
}

impl FromStr for OptionContract {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (rest, value) = option_contract(s).map_err(|_| ())?;
        if rest == "" {
            Ok(value)
        } else {
            Err(())
        }
    }
}

pub fn contract_date(input: &str) -> IResult<&str, ContractDate> {
    map(
        tuple((
            map_res(digit1, FromStr::from_str),
            month,
            map_res(digit1, FromStr::from_str),
        )),
        |(day, month, year)| ContractDate { day, month, year },
    )(input)
}

impl FromStr for ContractDate {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (rest, value) = contract_date(s).map_err(|_| ())?;
        if rest == "" {
            Ok(value)
        } else {
            Err(())
        }
    }
}

pub fn month(input: &str) -> IResult<&str, Month> {
    alt((
        map(tag("JAN"), |_| Month::January),
        map(tag("FEB"), |_| Month::February),
        map(tag("MAR"), |_| Month::March),
        map(tag("APR"), |_| Month::April),
        map(tag("MAY"), |_| Month::May),
        map(tag("JUN"), |_| Month::June),
        map(tag("JUL"), |_| Month::July),
        map(tag("AUG"), |_| Month::August),
        map(tag("SEP"), |_| Month::September),
        map(tag("OCT"), |_| Month::October),
        map(tag("NOV"), |_| Month::November),
        map(tag("DEC"), |_| Month::December),
    ))(input)
}

pub fn option_kind(input: &str) -> IResult<&str, OptionKind> {
    alt((
        map(tag("P"), |_| OptionKind::Put),
        map(tag("C"), |_| OptionKind::Call),
    ))(input)
}

impl FromStr for OptionKind {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (rest, value) = option_kind(s).map_err(|_| ())?;
        if rest == "" {
            Ok(value)
        } else {
            Err(())
        }
    }
}

impl Display for Currency {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(
            fmt,
            "{}",
            match self {
                USDT => "USDT",
                BTC => "BTC",
                ETH => "ETH",
            }
        )
    }
}

impl Display for InstrumentKind {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(
            fmt,
            "{}",
            match self {
                InstrumentKind::Option => "option",
                InstrumentKind::Future => "future",
            }
        )
    }
}

impl Debug for InstrumentKind {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}", self)
    }
}

impl Display for Instrument {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        match self {
            Instrument::Future(this) => Display::fmt(this, fmt),
            Instrument::Perpetual(this) => Display::fmt(this, fmt),
            Instrument::Option(this) => Display::fmt(this, fmt),
        }
    }
}

impl Serialize for Instrument {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.collect_str(self)
    }
}

impl Debug for Instrument {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}", self)
    }
}

impl Display for UnderlyingIndex {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        match self {
            UnderlyingIndex::Instrument(this) => Display::fmt(this, fmt),
            UnderlyingIndex::IndexPrice => write!(fmt, "index_price"),
        }
    }
}

impl Serialize for UnderlyingIndex {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.collect_str(self)
    }
}

impl Debug for UnderlyingIndex {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}", self)
    }
}

impl Display for FutureContract {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}-{}", self.currency, self.date,)
    }
}

impl Debug for FutureContract {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}", self)
    }
}

impl Display for PerpetualContract {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}-PERPETUAL", self.currency,)
    }
}

impl Serialize for PerpetualContract {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.collect_str(self)
    }
}

impl Debug for PerpetualContract {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}", self)
    }
}

impl Display for OptionContract {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(
            fmt,
            "{}-{}-{}-{}",
            self.currency, self.date, self.strike, self.kind
        )
    }
}

impl Serialize for OptionContract {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.collect_str(self)
    }
}

impl Debug for OptionContract {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}", self)
    }
}

impl OptionKind {
    pub const fn to_str(self) -> &'static str {
        match self {
            OptionKind::Put => "P",
            OptionKind::Call => "C",
        }
    }
}

impl Display for OptionKind {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}", self.to_str())
    }
}

impl Debug for OptionKind {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}", self)
    }
}

impl ContractDate {
    pub const fn get_month_str(month: Month) -> &'static str {
        use Month::*;
        match month {
            January => "JAN",
            February => "FEB",
            March => "MAR",
            April => "APR",
            May => "MAY",
            June => "JUN",
            July => "JUL",
            August => "AUG",
            September => "SEP",
            October => "OCT",
            November => "NOV",
            December => "DEC",
        }
    }
    pub const fn month_str(self) -> &'static str {
        Self::get_month_str(self.month)
    }
}

impl Display for ContractDate {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}{}{}", self.day, self.month_str(), self.year)
    }
}

impl Serialize for ContractDate {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.collect_str(self)
    }
}

impl<'de> Deserialize<'de> for ContractDate {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s: String = Deserialize::deserialize(deserializer)?;
        ContractDate::from_str(&*s)
            .map_err(|_| D::Error::custom(format!("Invalid contract date {:?}", s)))
    }
}

impl Debug for ContractDate {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}", self)
    }
}
