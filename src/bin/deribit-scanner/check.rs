use super::*;

pub async fn run_check(matches: &ArgMatches<'_>) {
    let tasks: Vec<_> = matches
        .values_of("INPUT")
        .expect("Must have at least one input file")
        .map(|path| tokio::spawn(check_path(path.to_owned())))
        .collect();
    for task in tasks {
        task.await
            .expect("Task failed to run")
            .expect("Task returned error")
    }
}

pub async fn check_path<P: AsRef<std::path::Path> + std::fmt::Display + Clone>(
    path: P,
) -> tokio::io::Result<()> {
    let mut file = tokio::fs::File::open(path.clone()).await?;
    let mut buf = Vec::with_capacity(1 << 16);
    let start = time::Instant::now();
    let _ = file.read_to_end(&mut buf).await?;
    let duration = start.elapsed();
    info!(
        "Loaded {} from {} in {:?} ({}/sec)",
        HumanBytes(buf.len()),
        path,
        duration,
        HumanBytes(buf.len() as f64 / duration.as_secs_f64())
    );
    let mut reader = &buf[..];
    let mut summaries: u64 = 0u64;
    let start = time::Instant::now();
    loop {
        match rmp_serde::decode::from_read::<_, BookSummary>(&mut reader) {
            Ok(_summary) => summaries += 1,
            Err(err) => {
                if reader.len() != 0 {
                    warn!(
                        "Invalid summary #{} in file {}, @ {} bytes from end: {}",
                        summaries,
                        path,
                        reader.len(),
                        err
                    );
                }
                break;
            }
        }
    }
    let duration = start.elapsed();
    println!(
        "Loaded {} valid summaries from file {} in {:?}, with {} left ({}% valid, {} summaries/sec, {}/sec, {}/summary)",
        summaries,
        path,
        duration,
        HumanBytes(reader.len()),
        100.0 - 100.0 * (reader.len() as f64 / buf.len() as f64),
        summaries as f64 / duration.as_secs_f64(),
        HumanBytes(buf.len() as f64 / duration.as_secs_f64()),
        HumanBytes(buf.len() as f64 / summaries as f64),
    );
    Ok(())
}
