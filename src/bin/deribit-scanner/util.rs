use std::str::FromStr;

use super::*;

/// A number of bytes displayed in a human readable way
pub struct HumanBytes<T>(pub T);

impl Display for HumanBytes<usize> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        if self.0 >= 1024 * 1024 * 102 {
            write!(f, "{:.4} GiB", self.0 as f64 / (1024 * 1024 * 1024) as f64)
        } else if self.0 >= 1024 * 102 {
            write!(f, "{:.4} MiB", self.0 as f64 / (1024 * 1024) as f64)
        } else if self.0 >= 1024 {
            write!(f, "{:.4} KiB", self.0 as f64 / 1024.0)
        } else {
            write!(f, "{} bytes", self.0)
        }
    }
}

impl Display for HumanBytes<u64> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        if self.0 >= 1024 * 1024 * 102 {
            write!(f, "{:.4} GiB", self.0 as f64 / (1024 * 1024 * 1024) as f64)
        } else if self.0 >= 1024 * 102 {
            write!(f, "{:.4} MiB", self.0 as f64 / (1024 * 1024) as f64)
        } else if self.0 >= 1024 {
            write!(f, "{:.4} KiB", self.0 as f64 / 1024.0)
        } else {
            write!(f, "{} bytes", self.0)
        }
    }
}

impl Display for HumanBytes<f64> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        if self.0 >= (1024 * 1024 * 102) as f64 {
            write!(f, "{:.4} GiB", self.0 as f64 / (1024 * 1024 * 1024) as f64)
        } else if self.0 >= (1024 * 102) as f64 {
            write!(f, "{:.4} MiB", self.0 as f64 / (1024 * 1024) as f64)
        } else if self.0 >= 1024 as f64 {
            write!(f, "{:.4} KiB", self.0 as f64 / 1024.0)
        } else {
            write!(f, "{:.4} bytes", self.0)
        }
    }
}

pub async fn run_summary(matches: &ArgMatches<'_>) {
    let input = if let Some(input) = matches.values_of("INPUT") {
        input
    } else {
        return;
    };
    let mut instruments = FxHashSet::with_capacity_and_hasher(input.len(), Default::default());
    let mut btc = false;
    let mut eth = false;
    for input in input {
        match Instrument::from_str(input) {
            Ok(instrument) => {
                match instrument.currency() {
                    BTC => btc = true,
                    ETH => eth = true,
                    _ => {}
                }
                if !instruments.insert(instrument) {
                    warn!("Duplicate instrument {:?}", instrument)
                }
            }
            _ => error!("Invalid instrument {:?}", input),
        }
    }
    if instruments.is_empty() {
        return;
    }
    let deribit = if matches.is_present("test") {
        Deribit::test()
    } else {
        Deribit::production()
    };
    if btc {
        let btc_summaries = deribit
            .get_book_summary_by_currency(BTC, None)
            .await
            .expect("Failed to connect to Deribit for BTC summaries");
        for summary in btc_summaries.result {
            if instruments.remove(&summary.instrument_name) {
                if let Err(err) = serde_json::to_writer_pretty(std::io::stdout(), &summary) {
                    error!(
                        "JSON summary serialization error: {}\nSummary: {:#?}",
                        err, summary
                    )
                }
            }
        }
    }
    if eth {
        let eth_summaries = deribit
            .get_book_summary_by_currency(ETH, None)
            .await
            .expect("Failed to connect to Deribit for ETH summaries");
        for summary in eth_summaries.result {
            if instruments.remove(&summary.instrument_name) {
                if let Err(err) = serde_json::to_writer_pretty(std::io::stdout(), &summary) {
                    error!(
                        "JSON summary serialization error: {}\nSummary: {:#?}",
                        err, summary
                    )
                }
            }
        }
    }
    for instrument in instruments {
        warn!("No data for instrument {}", instrument)
    }
}
