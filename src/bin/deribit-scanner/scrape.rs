use super::*;

pub async fn run_scrape(matches: &ArgMatches<'_>) {
    let output_dir: PathBuf = matches.value_of("output").unwrap_or(".").into();
    let mut btc_path = output_dir.clone();
    btc_path.push("btc_book_summary.messagepack");
    let mut eth_path = output_dir.clone();
    eth_path.push("eth_book_summary.messagepack");

    let mut options = tokio::fs::OpenOptions::new();
    options.append(true).create(true);
    let mut btc_file = options
        .open(btc_path)
        .await
        .expect("Failed to open btc file");
    let mut eth_file = options
        .open(eth_path)
        .await
        .expect("Failed to open eth file");

    let verbose = matches.is_present("verbose");
    let data = matches
        .value_of("data")
        .map(|data| data.parse().expect("Argument to -d must be a valid u64"))
        .unwrap_or(1024 * 1024);
    let max_retries = matches
        .value_of("max_retries")
        .map(|max_retries| {
            max_retries
                .parse()
                .expect("Argument to -t must be a valid u64")
        })
        .unwrap_or(3);
    let rate = matches
        .value_of("rate")
        .map(|rate| rate.parse().expect("Argument to -r must be a valid u64"))
        .unwrap_or(500);
    let config = Config {
        verbose,
        max_retries,
        data,
        initial_capacity: 2 << 16,
        rate: time::Duration::from_millis(rate),
    };
    let deribit = if matches.is_present("test") {
        Deribit::test()
    } else {
        Deribit::production()
    };
    let result = tokio::try_join!(
        repeated_scrape_top_of_book(&deribit, Currency::BTC, &mut btc_file, &config),
        repeated_scrape_top_of_book(&deribit, Currency::ETH, &mut eth_file, &config),
    );
    match result {
        Ok((btc, eth)) => info!(
            "Successfully captured {} of BTC data and {} of ETH data, for a total of {}",
            HumanBytes(btc),
            HumanBytes(eth),
            HumanBytes(btc + eth)
        ),
        Err((written, err)) => panic!("Failed after writing {}: {}", written, err),
    }
}

pub async fn repeated_scrape_top_of_book<W: AsyncWrite + Unpin>(
    deribit: &Deribit,
    currency: Currency,
    writer: &mut W,
    config: &Config,
) -> Result<u64, (u64, ScanError)> {
    let rate = config.rate;
    let mut interval = tokio::time::interval(rate);
    let data = config.data;
    let max_retries = config.max_retries;
    let mut tries = 0;
    let mut iter = 0;
    let mut previous = FxHashMap::default();
    let mut written = 0;
    let mut buf = Vec::with_capacity(config.initial_capacity);
    while tries <= max_retries && written < data {
        interval.tick().await;
        let scan = scrape_top_of_book(
            deribit,
            currency,
            &mut buf,
            iter,
            written,
            &mut previous,
            config,
        )
        .await;
        let result = match scan {
            Ok(_) => writer
                .write_all(&buf[..])
                .await
                .map_err(|err| ScanError::FileError(currency, err)),
            Err(err) => Err(err),
        };
        if result.is_ok() {
            written += buf.len() as u64;
        }
        buf.clear();
        match result {
            Ok(()) => tries = 0,
            Err(ScanError::ScannerError(currency, scan)) => {
                warn!(
                    "{}: error scanning. Try {}/{} (interval = {:?}). Error: {}",
                    currency, tries, max_retries, rate, scan,
                );
                if tries >= max_retries {
                    return Err((written, ScanError::ScannerError(currency, scan)));
                }
                tries += 1;
            }
            Err(err) => return Err((written, err)),
        }
        iter += 1;
    }
    Ok(written)
}

pub async fn scrape_top_of_book(
    deribit: &Deribit,
    currency: Currency,
    buf: &mut Vec<u8>,
    iter: u64,
    bytes: u64,
    previous: &mut FxHashMap<Instrument, BookSummary>,
    config: &Config,
) -> Result<Vec<BookSummary>, ScanError> {
    let start_len = buf.len();
    let mut updates = 0u64;
    let start_request = time::Instant::now();
    let summaries = deribit
        .get_book_summary_by_currency(currency, None)
        .await
        .map_err(|err| ScanError::ScannerError(currency, err))?
        .result;
    let start_storing = time::Instant::now();
    for summary in &summaries {
        let changed = match previous.entry(summary.instrument_name) {
            Entry::Occupied(occ) => {
                let previous = occ.into_mut();
                if previous.creation_timestamp < summary.creation_timestamp
                    && !previous.equivalent(summary)
                {
                    *previous = *summary;
                    true
                } else {
                    false
                }
            }
            Entry::Vacant(vac) => {
                vac.insert(*summary);
                true
            }
        };
        if changed {
            rmp_serde::encode::write(buf, summary)
                .map_err(|err| ScanError::SerializationError(currency, err))?;
            updates += 1;
        };
    }
    let done = time::Instant::now();
    let read = buf.len() - start_len;
    let request_time = start_storing - start_request;
    let storage_time = done - start_storing;
    let total_time = done - start_request;
    info!(
        "{} (iter = {}, {}% capacity): Processed {} updates for {} instruments in {:?} in {} = {}/sec\n({:?} request = {}/sec, {:?} storage = {}/sec, {}/summary stored, {}% updates)",
        currency,
        iter,
        100.0 * bytes as f64 / config.data as f64,
        updates,
        summaries.len(),
        total_time,
        HumanBytes(read),
        HumanBytes(read as f64 / total_time.as_secs_f64()),
        request_time,
        HumanBytes(read as f64 / request_time.as_secs_f64()),
        storage_time,
        HumanBytes(read as f64 / storage_time.as_secs_f64()),
        HumanBytes(read as f64 / summaries.len() as f64),
        100.0 * (updates as f64 / summaries.len() as f64),
    );
    Ok(summaries)
}
