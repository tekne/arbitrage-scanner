use arbitrage_scanner::deribit::{data::*, *};
use clap::{App, Arg, ArgMatches, SubCommand};
use fxhash::{FxHashMap, FxHashSet};
use log::*;
use std::collections::hash_map::Entry;
use std::fmt::{self, Display, Formatter};
use std::path::PathBuf;
use std::time;
use thiserror::Error;
use tokio::io::{AsyncReadExt, AsyncWrite, AsyncWriteExt};

#[derive(Error, Debug)]
pub enum ScanError {
    /// A scanner error
    #[error("Scanner error for {0}: {1}")]
    ScannerError(Currency, #[source] arbitrage_scanner::Error),
    /// A serialization error
    #[error("Serialization error for {0}: {1}")]
    SerializationError(Currency, #[source] rmp_serde::encode::Error),
    /// A file system error
    #[error("File system error for {0}: {1}")]
    FileError(Currency, #[source] tokio::io::Error),
}

#[derive(Debug)]
pub struct Config {
    verbose: bool,
    rate: time::Duration,
    max_retries: u64,
    data: u64,
    initial_capacity: usize,
}

mod boxes;
mod check;
mod scrape;
mod util;

use boxes::*;
use check::*;
use scrape::*;
use util::*;

pub async fn run_main(matches: &ArgMatches<'_>) {
    let default_matches = ArgMatches::default();
    match matches.subcommand() {
        ("check", matches) => run_check(matches.unwrap_or(&default_matches)).await,
        ("scrape", matches) => run_scrape(matches.unwrap_or(&default_matches)).await,
        ("boxes", matches) => run_boxes(matches.unwrap_or(&default_matches)).await,
        ("summary", matches) => run_summary(matches.unwrap_or(&default_matches)).await,
        (other, _) => panic!("Invalid subcommand {:?}", other),
    }
}

#[tokio::main]
async fn main() {
    let matches = App::new("deribit scraper")
        .version("0.0")
        .author("Jad Ghalayini")
        .about("Scans Deribit")
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .help("Whether to use verbose logging"),
        )
        .subcommand(
            SubCommand::with_name("scrape")
            .version("0.0")
            .about("Scrapes Deribit for top-of-book order data")
            .arg(
                Arg::with_name("output")
                    .short("o")
                    .long("output")
                    .value_name("FILE")
                    .help("The folder to output scraped data to. If None, uses the current directory")
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("test")
                    .long("test")
                    .help("Use the test deribit API"),
            )
            .arg(
                Arg::with_name("max_retries")
                    .short("t")
                    .long("max_retries")
                    .takes_value(true)
                    .help("Maximum retries on API failure. Default = 3"),
            )
            .arg(
                Arg::with_name("data").short("d")
                    .long("data")
                    .takes_value(true)
                    .help(
                        "Maximum bytes of data to collect, per currency. Default = 1048576 bytes (1 MB)",
                    ),
            )
            .arg(
                Arg::with_name("rate")
                    .short("r")
                    .long("rate")
                    .takes_value(true)
                    .help("Maximum request rate in ms. Default = 500 ms"),
            )
        )
        .subcommand(
            SubCommand::with_name("check")
                .about("checks a file contains valid messagepack book summaries")
                .version("0.0")
                .arg(Arg::with_name("INPUT")
                    .help("Sets the input files to check")
                    .required(true)
                    .multiple(true)
                    .takes_value(true)
                    .value_name("FILE")
                )
        )
        .subcommand(
            SubCommand::with_name("summary")
                .about("Gets the summary of the order book for the provided instruments")
                .version("0.0")
                .arg(Arg::with_name("INPUT")
                    .help("Sets the instruments to check")
                    .multiple(true)
                    .takes_value(true)
                )
                .arg(
                    Arg::with_name("test")
                        .long("test")
                        .help("Use the test deribit API"),
                )
        )
        .subcommand(

            SubCommand::with_name("boxes")
            .version("0.0")
            .arg(
                Arg::with_name("output")
                    .short("o")
                    .long("output")
                    .value_name("FILE")
                    .help("The folder to output scraped data to; if this flag is provided, data collected during operation will be scraped. Does nothing in the presence of the input flag")
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("input")
                    .short("i")
                    .long("input")
                    .value_name("FILE")
                    .help("Files to import scraped data from. If not provided, uses live data.")
                    .takes_value(true)
                    .multiple(true),
            )
            .arg(
                Arg::with_name("test")
                    .long("test")
                    .help("Use the test deribit API"),
            )
            .arg(
                Arg::with_name("max_retries")
                    .short("t")
                    .long("max_retries")
                    .takes_value(true)
                    .help("Maximum retries on API failure. Default = 3"),
            )
            .arg(
                Arg::with_name("data").short("d")
                    .long("data")
                    .takes_value(true)
                    .help(
                        "Maximum bytes of data to collect, per currency. Default = 1048576 bytes (1 MB)",
                    ),
            )
            .arg(
                Arg::with_name("rate")
                    .short("r")
                    .long("rate")
                    .takes_value(true)
                    .help("Maximum request rate in ms. Default = 500 ms"),
            )
        )
        .get_matches();

    let mut logger = pretty_env_logger::formatted_timed_builder();
    if matches.is_present("verbose") {
        logger.filter(None, log::LevelFilter::Info);
    }
    logger.init();

    run_main(&matches).await;
}
