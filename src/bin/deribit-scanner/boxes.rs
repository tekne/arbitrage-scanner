use ordered_float::NotNan;

use super::*;

pub async fn run_boxes(matches: &ArgMatches<'_>) {
    let deribit = if matches.is_present("test") {
        Deribit::test()
    } else {
        Deribit::production()
    };
    let epsilon = NotNan::default();
    //TODO: join
    let btc_summaries = deribit
        .get_book_summary_by_currency(BTC, None)
        .await
        .expect("Failed to connect to Deribit for BTC summaries");
    let eth_summaries = deribit
        .get_book_summary_by_currency(ETH, None)
        .await
        .expect("Failed to connect to Deribit for BTC summaries");
    let mut spreads: Vec<_> =
        arbitrage_scanner::deribit::data::box_spreads(BTC, btc_summaries.result.iter())
            .chain(arbitrage_scanner::deribit::data::box_spreads(
                ETH,
                eth_summaries.result.iter(),
            ))
            .filter_map(|spread| spread.profitable(epsilon).map(|profit| (profit, spread)))
            .collect();
    spreads.sort_unstable_by_key(|((_side, profit), _spread)| *profit);

    for ((direction, amount), spread) in &spreads {
        println!("Profit of {} by going {:?} in spread", amount, direction);
        println!("Expiration value: {}", spread.expiration_value());
        if let Err(err) = serde_json::to_writer_pretty(std::io::stdout(), &spread) {
            error!("Error serializing spread: {}\nSpread: {:#?}", err, spread)
        }
        println!("\n=================\n\n");
    }

    if spreads.is_empty() {
        println!("No profitable spreads detected!");
    }
}
