use thiserror::Error;

/// An error which may be thrown by this library
#[derive(Error, Debug)]
pub enum Error {
    /// An error requesting something from Deribit
    #[error("Deribit request error: {}", 0)]
    DeribitRequestError(#[source] reqwest::Error),
    /// A JSON deserialization error
    #[error("JSON deserialization error")]
    JSONDeserializationError(#[source] reqwest::Error),
}
